FROM gradle:jdk11 as builder
RUN mkdir -p /app/gradle app/src
COPY gradle /app/gradle
COPY src /app/src
COPY build.gradle gradlew settings.gradle /app/
WORKDIR /app
RUN gradle fullJar

FROM openjdk:11.0.7-jre
COPY --from=builder /app/build/libs/paging-mission-control-all.jar .
RUN mkdir /input
COPY input /input/
ENTRYPOINT [ "java", "-jar", "paging-mission-control-all.jar", "/input/input.txt" ]