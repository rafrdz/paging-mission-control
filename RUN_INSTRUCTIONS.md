# Run Instructions
This file specifies the versions of Java and Gradle used to build this application

## Java Version
AdoptOpenJDK 11.0.7

## Gradle Version
Gradle 6.5

### Running the Application

#### Gradle Run
Use `gradle run --args="input.txt"` to run the application using the existing `input.txt` file. A different file may be used provided the path of the file is substituted for the `input.txt` file.

#### JAR With All Dependencies
Use `gradle fullJar` to create the JAR, then use `java -jar build/libs/paging-mission-control-all.jar "path/to/input/file.txt"` to run the application.

#### Docker
* Place the `input.txt` file into the `input` directory *(NOTE: The image will try to run `input.txt` from the `input` directory so the input file must be named `input.txt`)*
* Use `docker build -t [image name] .` to build the image
* Use `docker run --rm [image name]` to run the container