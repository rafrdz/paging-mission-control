package paging.mission.control.service.impl;

import paging.mission.control.App;
import paging.mission.control.model.GsonSatelliteData;
import paging.mission.control.model.SatelliteData;
import paging.mission.control.service.DataService;
import paging.mission.control.thread.CheckLimit;

import java.util.*;


public class DataServiceImpl implements DataService {
    private static final int MINUTE_INTERVAL = 5;
    public static final Map<String, SatelliteData> syncMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * Parses a line from the input file and adds the data to the data map in the main class where each Satellite ID in
     * the overall data set is a key and the value for that key is a list of all SatelliteData objects with that same
     * key.
     *
     * @param line : A line of data from the input file
     */
    @Override
    public void parseData(String line) {
        String[] lineSplit = line.split("\\|");
        int id = Integer.parseInt(lineSplit[1]);
        SatelliteData newData = new SatelliteData(id, lineSplit[0], lineSplit[2], lineSplit[3], lineSplit[4],
                lineSplit[5], lineSplit[6], lineSplit[7]);
        if (!App.dataMap.containsKey(id)) {
            ArrayList<SatelliteData> newDataList = new ArrayList<>();
            newDataList.add(newData);
            App.dataMap.put(id, newDataList);
        } else {
            App.dataMap.get(id).add(newData);
        }
    }

    /**
     * Iterates through the data map from the App class and calls the process method on each list (value in the map)
     * of data. Then iterates through the synchronized map, populated by each thread, which contains all SatelliteData
     * objects that violated the given conditions.
     */
    @Override
    public void processData() {
        App.dataMap.forEach((k, v) -> splitDataListAndCreateThread(v));
        syncMap.forEach((k, v) -> App.gsonList.add(new GsonSatelliteData(v, k)));
    }

    /**
     * Takes in the entire list of SatelliteData objects for an individual satellite id, divides up the list based on
     * the specified minute interval (defined in a constant which can be changed), and creates a thread to process
     * that slice of the whole list. The logic in this method starts at the first element in the list, collects all
     * objects within a specific minute interval, then sends that collection to a thread to check for errors. Then it
     * starts from the second element in the list, collects all objects within the same minute interval, then sends
     * that collection to a thread to check for errors. This is repeated until all intervals have been covered.
     *
     * @param data: List of SatelliteData objects for an individual satellite id
     */
    private static void splitDataListAndCreateThread(List<SatelliteData> data) {
        List<SatelliteData> dataInInterval = new ArrayList<>();
        for(int i = 0; i < data.size(); i++) {
            int startMinute = data.get(i).getTimestamp().getMinute();
            dataInInterval.add(data.get(i));
            for(int j = i + 1; j < data.size(); j++) {
                if(data.get(j).getTimestamp().getMinute() <= startMinute + MINUTE_INTERVAL) {
                    dataInInterval.add(data.get(j));
                } else {
                    break;
                }
            }
            Thread thread = new Thread(new CheckLimit(dataInInterval));
            thread.start();
            try {
                thread.join();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
            dataInInterval.clear();
        }
    }
}
