package paging.mission.control.service;

public interface DataService {
    /**
     * Parses a line from the input file and adds the data to the data map in the App class
     *
     * @param line: A line of data from the input file
     */
    void parseData(String line);

    /**
     * Iterates through the data map from the App class and calls the process method on each list of data
     */
    void processData();
}
