package paging.mission.control.model;

/**
 * The GsonSatelliteData class represents a subset of SatelliteData that will be converted into a JSON object
 */
public class GsonSatelliteData {
    private int satelliteId;
    private String severity;
    private String component;
    private String timestamp;

    public GsonSatelliteData(SatelliteData data, String severity) {
        this.satelliteId = data.getId();
        this.severity = severity;
        this.component = data.getComponent();
        this.timestamp = data.getTimestamp().toString();
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GsonSatelliteData{");
        sb.append("satelliteId=").append(satelliteId);
        sb.append(", severity='").append(severity).append('\'');
        sb.append(", component='").append(component).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append('}');
        return sb.toString();
    }
}
