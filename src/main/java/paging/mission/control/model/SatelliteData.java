package paging.mission.control.model;

import paging.mission.control.util.TimeUtil;

import java.time.ZonedDateTime;

/**
 * The SatelliteData class represents data received from the satellite
 */
public class SatelliteData {
    private int id;
    private ZonedDateTime timestamp;
    private int redHigh;
    private int yellowHigh;
    private int yellowLow;
    private int redLow;
    private double rawValue;
    private String component;

    public SatelliteData(int id, String timestamp, String redHigh, String yellowHigh, String yellowLow, String redLow,
                         String rawValue, String component) {
        this.id = id;
        this.timestamp = TimeUtil.formatTime(timestamp);
        this.redHigh = Integer.parseInt(redHigh);
        this.yellowHigh = Integer.parseInt(yellowHigh);
        this.yellowLow = Integer.parseInt(yellowLow);
        this.redLow = Integer.parseInt(redLow);
        this.rawValue = Double.parseDouble(rawValue);
        this.component = component;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getRedHigh() {
        return redHigh;
    }

    public void setRedHigh(int redHigh) {
        this.redHigh = redHigh;
    }

    public int getYellowHigh() {
        return yellowHigh;
    }

    public void setYellowHigh(int yellowHigh) {
        this.yellowHigh = yellowHigh;
    }

    public int getYellowLow() {
        return yellowLow;
    }

    public void setYellowLow(int yellowLow) {
        this.yellowLow = yellowLow;
    }

    public int getRedLow() {
        return redLow;
    }

    public void setRedLow(int redLow) {
        this.redLow = redLow;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SatelliteData{");
        sb.append("id=").append(id);
        sb.append(", timestamp=").append(timestamp);
        sb.append(", redHigh=").append(redHigh);
        sb.append(", yellowHigh=").append(yellowHigh);
        sb.append(", yellowLow=").append(yellowLow);
        sb.append(", redLow=").append(redLow);
        sb.append(", rawValue=").append(rawValue);
        sb.append(", component='").append(component).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
