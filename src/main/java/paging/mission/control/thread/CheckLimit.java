package paging.mission.control.thread;

import paging.mission.control.model.SatelliteData;
import paging.mission.control.service.impl.DataServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Runnable class which contains the constant values and methods used to check a given list of SatelliteData objects
 * for any violations.
 */
public class CheckLimit implements Runnable {
    private static final String RED_HIGH = "RED HIGH";
    private static final String RED_LOW = "RED LOW";
    private static final int NUMBER_OF_BAD_READINGS = 3;

    private final List<SatelliteData> dataList;

    public CheckLimit(List<SatelliteData> dataList) {
        this.dataList = dataList;
    }

    @Override
    public void run() {
        // Run the data list through the check method and store the resulting map
        Map<String, SatelliteData> resultMap = checkListForErrors(dataList);

        // Iterate through the map and attempt to add each key, value pair to the synchronized set in the
        // DataServiceImpl class
        resultMap.forEach(DataServiceImpl.syncMap::put);
    }

    /**
     * Receives a subset containing SatelliteData objects within a 5 minute period (defined by a constant which can be
     * changed), and checks that subset for at least 3 (defined by a constant which can be changed) SatelliteData
     * objects with violations within the specified period of time.
     *
     * @param data List of SatelliteData objects that are all within a 5 minute period of time
     * @return Map of a Limit Error (String, will be HIGH or LOW letting us know which rule was violated) and a
     * SatelliteData object
     */
    private Map<String, SatelliteData> checkListForErrors(List<SatelliteData> data) {
        Map<String, SatelliteData> errorMap = new HashMap<>();
        List<SatelliteData> lowErrors = new ArrayList<>();
        List<SatelliteData> highErrors = new ArrayList<>();
        for(SatelliteData d : data) {
            if(d.getRawValue() < d.getRedLow()) {
                lowErrors.add(d);
            } else if(d.getRawValue() > d.getRedHigh()) {
                highErrors.add(d);
            }
        }
        if(lowErrors.size() == NUMBER_OF_BAD_READINGS) {
            errorMap.put(RED_LOW, lowErrors.get(0));
        }
        if(highErrors.size() == NUMBER_OF_BAD_READINGS) {
            errorMap.put(RED_HIGH, highErrors.get(0));
        }
        return errorMap;
    }
}
