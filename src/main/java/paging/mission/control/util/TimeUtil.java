package paging.mission.control.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class containing utility methods for dealing with time
 */
public class TimeUtil {
    /**
     * Utility method used to format a string into a ZonedDateTime object
     *
     * @param time: String representation of a timestamp
     * @return ZonedDateTime object
     */
    public static ZonedDateTime formatTime(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        LocalDateTime dateTime = LocalDateTime.parse(time, formatter);
        return dateTime.atZone(ZoneOffset.UTC);
    }
}
